﻿using System;

namespace BooksApi.Exceptions
{
    public class ServiceException : Exception
    {
        public string Code { get; set; }

        public ServiceException(string code)
        {
            Code = code;
        }
    }
}
