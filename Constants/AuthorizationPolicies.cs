﻿namespace BooksApi.Constants
{
    public static class AuthorizationPolicies
    {
        public const string AdminPolicy = "AdminPolicy";

        public const string CustomerPolicy = "CustomerPolicy";
    }
}
