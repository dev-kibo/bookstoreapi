﻿namespace BooksApi.Constants
{
    public static class ServiceExceptionMessages
    {
        public const string SessionNotFound = "Session not found.";

        public const string UserNotFound = "User not found.";

        public const string UserEmailOrPasswordIsWrong = "User credentials don't exist.";

        public const string UserEmailInUse = "User email is already in use.";

        public const string UserUsernameInUse = "Username is already in use.";

        public const string ImageNotValid = "Image is not of a valid image type.";

        public const string ImageUploadFailed = "Image could not be uploaded.";

        public const string ImageNotValidBase64String = "Image is not a valid Base64 string.";

        public const string ImageNotFound = "Image is not found.";

        public const string UserUpdateFailed = "User could not be updated.";

        public const string BookNotFound = "Book does not exist.";

        public const string BookExists = "Book already exists.";

        public const string BookOutOfStock = "Book is out of stock.";

        public const string AuthorNotFound = "Author does not exist.";

        public const string CouponNotFound = "Coupon does not exist.";

        public const string CartNotFound = "Cart does not exist.";

        public const string CartExists = "Cart already exists.";

    }
}
