﻿namespace BooksApi.Constants
{
    public static class AuthenticationHeaders
    {
        public const string AccessToken = "Access-Token";

        public const string RefreshToken = "Refresh-Token";
    }
}
