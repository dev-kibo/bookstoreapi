﻿using BooksApi.Requests.CartBook;
using FluentValidation;

namespace BooksApi.Validation
{
    public class PutCartBookValidator : AbstractValidator<PutCartBookRequest>
    {
        public PutCartBookValidator()
        {
            RuleFor(cb => cb.BookId).NotNull();
            RuleFor(cb => cb.Quantity).NotNull().Custom((quantity, context) =>
            {
                if (!(quantity.HasValue && quantity.Value % 2 != 0) || quantity.Value < 1)
                {
                    context.AddFailure("Quantity must be a positive non decimal number.");
                }
            });
        }
    }
}
