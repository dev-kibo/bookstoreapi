﻿using BooksApi.Requests;
using FluentValidation;

namespace BooksApi.Validation
{
    public class PutAuthorValidator : AbstractValidator<PutAuthorRequest>
    {
        public PutAuthorValidator()
        {
            RuleFor(a => a.FirstName).NotEmpty().NotNull();
            RuleFor(a => a.LastName).NotEmpty().NotNull();
        }
    }
}
