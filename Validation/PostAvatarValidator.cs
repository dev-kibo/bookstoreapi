﻿using BooksApi.Requests;
using FluentValidation;

namespace BooksApi.Validation
{
    public class PostAvatarValidator : AbstractValidator<PostAvatarRequest>
    {
        public PostAvatarValidator()
        {
            RuleFor(a => a.Avatar).NotNull().NotEmpty();
        }
    }
}
