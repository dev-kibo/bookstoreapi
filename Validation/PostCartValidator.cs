﻿using BooksApi.Requests;
using FluentValidation;

namespace BooksApi.Validation
{
    public class PostCartValidator : AbstractValidator<PostCartRequest>
    {
        public PostCartValidator()
        {
            RuleFor(c => c.UserId).NotNull();
        }
    }
}
