﻿using BooksApi.Responses.Book;
using FluentValidation;

namespace BooksApi.Validation
{
    public class PostBookValidator : AbstractValidator<PostBookRequest>
    {
        public PostBookValidator()
        {
            RuleFor(b => b.Title).NotNull().NotEmpty();

            RuleFor(b => b.Isbn).NotNull().NotEmpty().Length(10, 13);

            RuleFor(b => b.Price).NotNull();

            RuleFor(b => b.Description).NotNull().NotEmpty();

            RuleFor(b => b.NumberOfPages).NotNull();

            RuleFor(b => b.Stock).NotNull();

            RuleFor(b => b.Status).NotNull();

            RuleFor(b => b.PublishedAt).NotNull();

            RuleFor(b => b.Author).NotNull();

            RuleFor(b => b.Author.FirstName).NotNull().NotEmpty();

            RuleFor(b => b.Author.LastName).NotNull().NotEmpty();
        }
    }
}
