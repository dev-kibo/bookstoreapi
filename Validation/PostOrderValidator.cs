﻿using BooksApi.Requests;
using FluentValidation;

namespace BooksApi.Validation
{
    public class PostOrderValidator : AbstractValidator<PostOrderRequest>
    {
        public PostOrderValidator()
        {
            RuleFor(o => o.UserId).NotNull();
            RuleFor(o => o.CartId).NotNull();
        }
    }
}
