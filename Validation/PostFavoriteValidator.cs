﻿using BooksApi.Requests;
using FluentValidation;

namespace BooksApi.Validation
{
    public class PostFavoriteValidator : AbstractValidator<PostFavoriteRequest>
    {
        public PostFavoriteValidator()
        {
            RuleFor(f => f.BookId).NotNull();
            RuleFor(f => f.UserId).NotNull();
        }
    }
}
