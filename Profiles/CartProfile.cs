﻿using AutoMapper;
using BooksApi.Models;
using BooksApi.Requests;
using BooksApi.Requests.Cart;
using BooksApi.Responses;

namespace BooksApi.Profiles
{
    public class CartProfile : Profile
    {
        public CartProfile()
        {
            CreateMap<Cart, GetCartResponse>();
            CreateMap<PostCartRequest, Cart>();
            CreateMap<PutCartRequest, Cart>();
        }
    }
}
