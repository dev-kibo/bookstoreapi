﻿using AutoMapper;
using BooksApi.Models;
using BooksApi.Responses;

namespace BooksApi.Profiles
{
    public class OrderProfile : Profile
    {
        public OrderProfile()
        {
            CreateMap<Order, GetOrderResponse>()
                .ForMember(gor => gor.CartItems, mapper => mapper.MapFrom(o => o.Cart.CartBooks));
        }
    }
}
