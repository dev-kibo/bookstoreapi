﻿using AutoMapper;
using BooksApi.Models;
using BooksApi.Responses;
using BooksApi.Services;

namespace BooksApi.Profiles.Resolvers
{
    public class AvatarResolver : IValueResolver<ApplicationUser, GetUserResponse, string>
    {
        private readonly ImageService imageService;

        public AvatarResolver(ImageService imageService)
        {
            this.imageService = imageService;
        }
        public string Resolve(ApplicationUser source, GetUserResponse destination, string destMember, ResolutionContext context)
        {
            var base64StringImage = imageService.ConvertUserAvatarToBase64String(source.Avatar);

            return base64StringImage == null ? null : base64StringImage;
        }
    }
}
