﻿using AutoMapper;
using BooksApi.Models;
using BooksApi.Requests;
using BooksApi.Responses;

namespace BooksApi.Profiles
{
    public class FavoriteProfile : Profile
    {
        public FavoriteProfile()
        {
            CreateMap<Favorite, GetFavoriteResponse>();
            CreateMap<PostFavoriteRequest, Favorite>();
        }
    }
}
