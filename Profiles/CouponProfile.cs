﻿using AutoMapper;
using BooksApi.Models;
using BooksApi.Requests;
using BooksApi.Responses;

namespace BooksApi.Profiles
{
    public class CouponProfile : Profile
    {
        public CouponProfile()
        {
            CreateMap<Coupon, GetCouponResponse>();
            CreateMap<PostCouponRequest, Coupon>();
        }
    }
}
