﻿using System;

namespace BooksApi.Models
{
    public class StripeSettings
    {
        private string secretKey;

        private string publicKey;

        private string currency;

        public string SecretKey
        {
            get => secretKey;
            set => _ = value == null || value.Length == 0 ? throw new ArgumentNullException() : secretKey = value;
        }

        public string PublicKey
        {
            get => publicKey;
            set => _ = value == null || value.Length == 0 ? throw new ArgumentNullException() : publicKey = value;
        }

        public string Currency
        {
            get => currency;
            set => _ = value == null || value.Length == 0 ? throw new ArgumentNullException() : currency = value;
        }
    }
}
