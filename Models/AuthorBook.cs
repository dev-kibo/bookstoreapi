﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BooksApi.Models
{
    public class AuthorBook
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [ForeignKey(nameof(Book))]
        [Required]
        public int BookId { get; set; }
        public Book Book { get; set; }

        [ForeignKey(nameof(Author))]
        [Required]
        public int AuthorId { get; set; }
        public Author Author { get; set; }
    }
}
