﻿using System.Collections.Generic;

namespace BooksApi.Models
{
    public class CustomerWithOrders
    {
        public ApplicationUser Customer { get; set; }

        public IEnumerable<CartBook> Orders { get; set; }
    }
}
