﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BooksApi.Models
{
    public class Book
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [Required]
        public string Title { get; set; }
        [Required]
        public string Isbn { get; set; }
        [Required]
        [Column(TypeName = "decimal(19,2)")]
        public decimal Price { get; set; }
        [Required]
        public string Description { get; set; }

        public string Image { get; set; }
        [Required]
        public int NumberOfPages { get; set; }
        [Required]
        public int Stock { get; set; }
        [Required]
        public BookStatus Status { get; set; }
        [Required]
        public DateTime PublishedAt { get; set; }
        // Default value set in context
        [Required]
        public DateTime CreatedAt { get; set; }

        public ICollection<AuthorBook> AuthorBooks { get; set; }

        public ICollection<Favorite> Favorites { get; set; }

        public ICollection<CartBook> CartBooks { get; set; }
    }
}
