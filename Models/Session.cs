﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BooksApi.Models
{
    public class Session
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [ForeignKey(nameof(User))]
        [Required]
        public int UserId { get; set; }
        public ApplicationUser User { get; set; }

        public string Client { get; set; }
        [Required]
        [MaxLength(32)]
        public byte[] RefreshTokenHash { get; set; }
        [Required]
        public DateTime RefreshedAt { get; set; }
        // Default value set in context
        [Required]
        public DateTime CreatedAt { get; set; }
    }
}
