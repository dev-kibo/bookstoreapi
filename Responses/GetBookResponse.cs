﻿using BooksApi.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;

namespace BooksApi.Responses.Book
{
    public class GetBookResponse
    {
        public int Id { get; set; }

        public string Title { get; set; }

        public string Isbn { get; set; }

        public decimal Price { get; set; }

        public string Description { get; set; }

        public string Image { get; set; }

        public int NumberOfPages { get; set; }

        public int Stock { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        public BookStatus Status { get; set; }

        public DateTime PublishedAt { get; set; }

        public IEnumerable<GetAuthorResponse> Authors { get; set; }
    }
}
