﻿using System;

namespace BooksApi.Responses
{
    public class GetCouponResponse
    {
        public int Id { get; set; }

        public GetUserResponse User { get; set; }

        public int ValidNum { get; set; }

        public decimal ProcentOff { get; set; }

        public DateTime CreatedAt { get; set; }
    }
}
