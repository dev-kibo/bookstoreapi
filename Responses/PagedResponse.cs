﻿using BooksApi.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Collections.Generic;

namespace BooksApi.Responses
{
    public class PagedResponse<T>
    {
        public PagedResponse() { }

        public PagedResponse(
            IEnumerable<T> data,
            int page,
            int results,
            int pageSize,
            int totalPages,
            BookSortingType? order = null,
            BookOrderingType? orderBy = null)
        {
            Data = data;
            Page = page;
            Results = results;
            PageSize = pageSize;
            TotalPages = totalPages;
            Order = order;
            OrderBy = orderBy;
        }

        public IEnumerable<T> Data { get; set; }

        public int Page { get; set; }

        public int Results { get; set; }

        public int PageSize { get; set; }

        public int TotalPages { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        public BookSortingType? Order { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        public BookOrderingType? OrderBy { get; set; }

    }
}
