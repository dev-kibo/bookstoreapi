﻿namespace BooksApi.Responses
{
    public class GetAuthenticationResponse
    {
        public string AccessToken { get; set; }

        public string RefreshToken { get; set; }
    }
}
