﻿namespace BooksApi.Responses
{
    public class GetUserResponse
    {
        public int Id { get; set; }

        public string Username { get; set; }

        public string Email { get; set; }

        public string Avatar { get; set; }
    }
}
