﻿namespace BooksApi.Responses
{
    public class GetExceptionResponse
    {
        public string Code { get; set; }

        public GetExceptionResponse(string code)
        {
            Code = code;
        }
    }
}
