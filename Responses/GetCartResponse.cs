﻿using BooksApi.Enums;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace BooksApi.Responses
{
    public class GetCartResponse
    {
        public int Id { get; set; }

        public GetUserResponse User { get; set; }

        public GetCouponResponse Coupon { get; set; }
        [JsonConverter(typeof(StringEnumConverter))]
        public CartStatus Status { get; set; }
    }
}
