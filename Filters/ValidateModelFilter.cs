﻿using BooksApi.Models;
using BooksApi.Responses;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System.Linq;
using System.Threading.Tasks;

namespace BooksApi.Filters
{
    public class ValidateModelFilter : IAsyncActionFilter
    {
        public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            if (!context.ModelState.IsValid)
            {
                var errors = context.ModelState.Keys
                    .Where(k => context.ModelState[k].Errors.Count > 0)
                    .Select(k =>
                    {
                        return context.ModelState[k].Errors.Select(e => new ValidationMessage(k, e.ErrorMessage));
                    })
                    .ToList();

                context.Result = new JsonResult(new GetValidationResponse(errors));
                return;
            }

            await next();
        }
    }
}
