﻿namespace BooksApi.Models
{
    public class SortQuery
    {

        public SortQuery()
        {
            SortBy = BookSortingType.Desc;
            OrderBy = BookOrderingType.Isbn;
        }
        public SortQuery(BookSortingType sortBy, BookOrderingType orderBy)
        {
            SortBy = sortBy;
            OrderBy = orderBy;
        }

        public BookSortingType SortBy { get; set; }

        public BookOrderingType OrderBy { get; set; }
    }
}
