﻿namespace BooksApi.Models
{
    public class PaginationQuery
    {

        public PaginationQuery()
        {
            Page = 1;
            PerPage = 5;
        }
        public PaginationQuery(int page, int perPage)
        {
            Page = page < 1 ? 1 : page;

            if (perPage < 5) PerPage = 5;
            else if (perPage > 25) PerPage = 25;
            else PerPage = perPage;
        }

        public int Page { get; set; }

        public int PerPage { get; set; }

    }
}
