﻿namespace BooksApi.Enums
{
    public enum CartStatus
    {
        Unordered, Ordered
    }
}
