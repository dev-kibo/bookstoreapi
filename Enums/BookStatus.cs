﻿namespace BooksApi.Models
{
    public enum BookStatus
    {
        Available, Unavailable
    }
}
