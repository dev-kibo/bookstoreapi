﻿namespace BooksApi.Models
{
    public enum BookOrderingType
    {
        Isbn, Title, Price, Stock, Status, PublishedAt
    }
}
