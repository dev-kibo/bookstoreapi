﻿using BooksApi.Data;
using BooksApi.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;

namespace BooksApi.Extensions
{
    public static class IServiceCollectionIdentityExtension
    {
        public static void AddIdentityExtension(this IServiceCollection services)
        {
            services.AddIdentityCore<ApplicationUser>(options =>
            {
                options.User.RequireUniqueEmail = true;
                options.User.AllowedUserNameCharacters = null;

                options.Password.RequireDigit = false;
                options.Password.RequireLowercase = false;
                options.Password.RequiredUniqueChars = 0;
                options.Password.RequireNonAlphanumeric = false;
                options.Password.RequireUppercase = false;
            })
                .AddRoles<IdentityRole<int>>()
                .AddDefaultTokenProviders()
                .AddEntityFrameworkStores<BooksApiContext>();
        }
    }
}
