﻿using BooksApi.Middleware;
using Microsoft.AspNetCore.Builder;

namespace BooksApi.Extensions
{
    public static class IApplicationBuilderResponseHeaderConfigurationMiddlewareExtension
    {
        public static IApplicationBuilder UseResponseHeaderConfiguration(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<ResponseHeaderConfigurationMiddleware>();
        }
    }
}
