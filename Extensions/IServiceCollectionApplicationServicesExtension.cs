﻿using BooksApi.Models;
using BooksApi.Services;
using BooksApi.Utilites;
using Microsoft.Extensions.DependencyInjection;
using MimeKit;
using Stripe;

namespace BooksApi.Extensions
{
    public static class IServiceCollectionApplicationServicesExtension
    {
        public static IServiceCollection AddApplicationServices(this IServiceCollection services)
        {
            services.AddScoped<BookService>();
            services.AddScoped<QueryService>();
            services.AddScoped<AuthorService>();
            services.AddScoped<AuthorBooksService>();
            services.AddScoped<FavoriteService>();
            services.AddScoped<UserService>();
            services.AddScoped<BooksApiCouponService>();
            services.AddScoped<CartService>();
            services.AddScoped<BooksApiOrderService>();
            services.AddScoped<CartBookService>();
            services.AddScoped<ImageService>();
            services.AddScoped<EmailSenderService>();
            services.AddScoped<EmailMessage>();
            services.AddScoped<MimeMessage>();
            services.AddScoped<RazorViewToStringRenderer>();
            services.AddScoped<BooksApiTokenService>();
            services.AddScoped<AuthenticationService>();
            services.AddScoped<SessionService>();
            services.AddScoped<HashService>();
            services.AddScoped<CustomerService>();
            services.AddScoped<PaymentIntentService>();
            services.AddScoped<ChargeService>();
            services.AddScoped<PaymentService>();

            return services;
        }
    }
}
