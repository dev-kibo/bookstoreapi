﻿using BooksApi.Constants;
using BooksApi.Requests;
using BooksApi.Requests.Cart;
using BooksApi.Requests.CartBook;
using BooksApi.Responses;
using BooksApi.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Net.Mime;
using System.Threading.Tasks;

namespace BooksApi.Controllers
{
    [ApiController]
    [Route("api/carts")]
    [Authorize(Policy = AuthorizationPolicies.CustomerPolicy)]
    [Produces(MediaTypeNames.Application.Json)]
    public class CartController : ControllerBase
    {
        private readonly CartService cartService;
        private readonly CartBookService cartBookService;

        public CartController(CartService cartService, CartBookService cartBookService)
        {
            this.cartService = cartService;
            this.cartBookService = cartBookService;
        }
        /// <summary>
        /// Returns the cart that matches unique id.
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// 
        ///     GET /carts/4
        /// 
        /// </remarks>
        /// <param name="id">Unique id of the cart</param>
        /// <response code="200">Returns cart that matched unique id</response>
        /// <response code="404">If cart not found.</response>       
        [HttpGet("{id:int}", Name = nameof(GetCartByIdAsync))]
        [SwaggerResponse(StatusCodes.Status200OK, Type = typeof(GetCartResponse))]
        [SwaggerResponse(StatusCodes.Status404NotFound, Type = typeof(GetExceptionResponse))]
        public async Task<ActionResult<GetCartResponse>> GetCartByIdAsync(int id)
        {
            if (await cartService.GetByIdAsync(id) == null)
            {
                return NotFound();
            }

            return Ok(await cartService.GetByIdAsync(id));
        }
        /// <summary>
        /// Returns items for the cart that matches unique Id.
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// 
        ///     GET /carts/4/items
        /// 
        /// </remarks>
        /// <param name="id">Unique id of the cart</param>
        /// <response code="200">Returns items for the cart that matched unique id</response>
        /// <response code="404">If items for the cart or cart not found.</response>       
        [HttpGet("{id:int}/items", Name = nameof(GetCartItemsAsync))]
        [SwaggerResponse(StatusCodes.Status200OK, Type = typeof(GetCartBookResponse))]
        [SwaggerResponse(StatusCodes.Status404NotFound, Type = typeof(GetExceptionResponse))]
        public async Task<ActionResult<IEnumerable<GetCartBookResponse>>> GetCartItemsAsync(int id)
        {
            var response = await cartBookService.GetAsync(id);

            if (response == null)
            {
                return NotFound();
            }

            return Ok(response);
        }
        /// <summary>
        /// Returns the item that matched unique Id from a cart that matched unique id.
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// 
        ///     GET /carts/4/items/4
        /// 
        /// </remarks>
        /// <param name="id">Unique id of the cart</param>
        /// <param name="itemId">Unique id of the item</param>
        /// <response code="200">Returns item that matched item unique id from the cart that matched cart unique id</response>
        /// <response code="404">If item not found.</response>       
        [HttpGet("{id:int}/items/{itemId}", Name = nameof(GetCartItemsByIdAsync))]
        [SwaggerResponse(StatusCodes.Status200OK, Type = typeof(GetCartBookResponse))]
        [SwaggerResponse(StatusCodes.Status404NotFound, Type = typeof(GetExceptionResponse))]
        public async Task<ActionResult<IEnumerable<GetCartBookResponse>>> GetCartItemsByIdAsync(int id, int itemId)
        {
            var response = await cartBookService.GetByIdAsync(id, itemId);

            if (response == null)
            {
                return NotFound();
            }

            return Ok(response);
        }
        /// <summary>
        /// Create a cart. Coupon is optional.
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// 
        ///     POST /carts
        ///     {
        ///         "couponId" : null,
        ///         "userId": 1
        ///     }
        /// 
        /// </remarks>
        /// <param name="newCart">Data for the book</param>
        /// <response code="201">Returns newly created cart/</response>
        /// <response code="409">If coupon or cart not found or if cart already exists.</response>        
        [HttpPost]
        [SwaggerResponse(StatusCodes.Status201Created, Type = typeof(GetCartResponse))]
        [SwaggerResponse(StatusCodes.Status409Conflict, Type = typeof(GetExceptionResponse))]
        public async Task<ActionResult<GetCartResponse>> PostAsync([Required] PostCartRequest newCart)
        {
            var res = await cartService.AddAsync(newCart);

            return CreatedAtRoute(nameof(GetCartByIdAsync), new { res.Id }, res);
        }
        /// <summary>
        /// Create a cart item.
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// 
        ///     POST /carts/4/items
        ///     {
        ///         "bookId" : 4,
        ///         "quantity": 10
        ///     }
        /// 
        /// </remarks>
        /// <param name="id">Unique id of the cart</param>
        /// <param name="newItem">Data for the cart item</param>
        /// <response code="201">Returns newly created cart item/</response>
        /// <response code="404">If cart not found.</response>
        /// <response code="409">If book or cart not found.</response>     
        [SwaggerResponse(StatusCodes.Status201Created, Type = typeof(GetCartBookResponse))]
        [SwaggerResponse(StatusCodes.Status404NotFound)]
        [SwaggerResponse(StatusCodes.Status409Conflict, Type = typeof(GetExceptionResponse))]
        [HttpPost("{id:int}/items")]
        public async Task<ActionResult<GetCartBookResponse>> PostItemToCartAsync(int id, [Required] PostCartBookRequest newItem)
        {
            if (await cartService.GetByIdAsync(id) == null)
            {
                return NotFound();
            }

            var response = await cartBookService.AddAsync(id, newItem);

            return CreatedAtRoute(nameof(GetCartItemsByIdAsync), new { id, ItemId = response.Id }, response);
        }
        /// <summary>
        /// Updates a cart item.
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// 
        ///     PUT /carts/4/items/4
        ///     {
        ///         "bookId" : 4,
        ///         "quantity": 15
        ///     }
        /// 
        /// </remarks>
        /// <param name="id">Unique id of the cart.</param>
        /// <param name="itemId">Unique id of the item.</param>
        /// <param name="updatedItem">Data for the cart item.</param>
        /// <response code="204">Returns nothing.</response>
        /// <response code="404">If cart or cart item not found.</response>
        [HttpPut("{id:int}/items/{itemId}")]
        [SwaggerResponse(StatusCodes.Status204NoContent)]
        [SwaggerResponse(StatusCodes.Status404NotFound)]
        public async Task<ActionResult> PutCartItemAsync(int id, int itemId, [Required] PutCartBookRequest updatedItem)
        {
            if (await cartService.GetByIdAsync(id) == null || await cartBookService.GetByIdAsync(id, itemId) == null)
            {
                return NotFound();
            }

            await cartBookService.UpdateAsync(id, itemId, updatedItem);

            return NoContent();
        }
        /// <summary>
        /// Deletes a cart item from the cart.
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// 
        ///     DELETE /carts/4/items/4
        /// 
        /// </remarks>
        /// <param name="id">Unique id of the cart.</param>
        /// <param name="itemId">Unique id of the item.</param>
        /// <response code="204">Returns nothing.</response>
        /// <response code="404">If cart or cart item not found.</response>
        [SwaggerResponse(StatusCodes.Status204NoContent)]
        [SwaggerResponse(StatusCodes.Status404NotFound)]
        [HttpDelete("{id:int}/items/{itemId}")]
        public async Task<ActionResult> DeleteCartItemAsync(int id, int itemId)
        {
            if (await cartService.GetByIdAsync(id) == null || await cartBookService.GetByIdAsync(id, itemId) == null)
            {
                return NotFound();
            }

            await cartBookService.DeleteAsync(id, itemId);

            return NoContent();
        }
        /// <summary>
        /// Updates a cart. Coupon is optional.
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// 
        ///     PUT /carts/4
        ///     {
        ///         "userId" : 4,
        ///         "couponId": 15
        ///     }
        /// 
        /// </remarks>
        /// <param name="id">Unique id of the cart.</param>
        /// <param name="updatedCart">Data for the cart.</param>
        /// <response code="204">Returns nothing.</response>
        /// <response code="404">If cart not found.</response>
        /// <response code="409">If user or coupon not found.</response>
        [HttpPut("{id:int}")]
        [SwaggerResponse(StatusCodes.Status204NoContent)]
        [SwaggerResponse(StatusCodes.Status404NotFound)]
        [SwaggerResponse(StatusCodes.Status409Conflict, Type = typeof(GetExceptionResponse))]
        public async Task<ActionResult> PutAsync(int id, [Required] PutCartRequest updatedCart)
        {
            if (await cartService.GetByIdAsync(id) == null)
            {
                return NotFound();
            }

            await cartService.UpdateAsync(id, updatedCart);

            return NoContent();
        }
        /// <summary>
        /// Deletes a coupon for a cart with unique id.
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// 
        ///     DELETE /carts/4/coupon
        /// 
        /// </remarks>
        /// <param name="id">Unique id of the cart.</param>
        /// <response code="204">Returns nothing.</response>
        /// <response code="404">If cart not found.</response>
        [HttpDelete("{id:int}/coupon")]
        [SwaggerResponse(StatusCodes.Status204NoContent)]
        [SwaggerResponse(StatusCodes.Status404NotFound)]
        public async Task<ActionResult> DeleteCouponFromCartAsync(int id)
        {
            if (await cartService.GetByIdAsync(id) == null)
            {
                return NotFound();
            }

            await cartService.DeleteCouponFromCartAsync(id);

            return NoContent();
        }
        /// <summary>
        /// Deletes a cart with unique id.
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// 
        ///     DELETE /carts/4
        /// 
        /// </remarks>
        /// <param name="id">Unique id of the cart.</param>
        /// <response code="204">Returns nothing.</response>
        /// <response code="404">If cart not found.</response>
        [HttpDelete("{id:int}")]
        [SwaggerResponse(StatusCodes.Status204NoContent)]
        [SwaggerResponse(StatusCodes.Status404NotFound)]
        public async Task<ActionResult> DeleteAsync(int id)
        {
            if (await cartService.GetByIdAsync(id) == null)
            {
                return NotFound();
            }

            await cartService.DeleteAsync(id);

            return NoContent();
        }

    }
}
