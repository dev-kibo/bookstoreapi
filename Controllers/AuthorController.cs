﻿using BooksApi.Constants;
using BooksApi.Requests;
using BooksApi.Responses;
using BooksApi.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;

namespace BooksApi.Controllers
{
    [Route("api/authors")]
    [ApiController]
    [Authorize(Policy = AuthorizationPolicies.AdminPolicy)]
    [Produces(MediaTypeNames.Application.Json)]
    public class AuthorController : ControllerBase
    {
        private readonly AuthorService authorService;

        public AuthorController(AuthorService authorService)
        {
            this.authorService = authorService;
        }
        /// <summary>
        /// Returns list of authors
        /// </summary>
        /// <remarks>
        ///     GET  /authors
        /// </remarks>
        /// <returns>List of authors</returns>
        /// <response code="200">Returns list of authors</response>
        /// <response code="404">If there are no authors.</response>
        [HttpGet]
        [AllowAnonymous]
        [SwaggerResponse(StatusCodes.Status200OK, Type = typeof(GetAuthorResponse))]
        [SwaggerResponse(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<PagedResponse<GetAuthorResponse>>> Get()
        {
            var res = await authorService.GetAsync();

            if (res.ToList().Count < 1)
            {
                return NotFound();
            }

            return Ok(res);
        }
        /// <summary>
        /// Returns the author that has a provided Id
        /// </summary>
        /// <remarks>
        ///     GET /authors/5
        ///     
        /// </remarks>
        /// <returns>Author that has an Id which was provided</returns>
        /// <param name="id">Id of an author</param>
        /// <response code="200">Returns author that has an Id which was provided</response>
        /// <response code="404">If author was not found.</response>        
        [HttpGet("{id:int}", Name = nameof(GetAuthorByIdAsync))]
        [AllowAnonymous]
        [SwaggerResponse(StatusCodes.Status200OK, Type = typeof(GetAuthorResponse))]
        [SwaggerResponse(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<GetAuthorResponse>> GetAuthorByIdAsync(int id)
        {
            if (await authorService.GetByIdAsync(id) == null)
            {
                return NotFound();
            }

            var res = await authorService.GetByIdAsync(id);

            return Ok(res);
        }
        /// <summary>
        /// Updates author whose Id is provided
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// 
        ///     PUT /authors/5
        ///     {
        ///         "firstName" : "George R.R.",
        ///         "lastName" : "Martin"
        ///     }
        ///     
        /// </remarks>
        /// <returns>Returns nothing</returns>
        /// <param name="id">Id of an author</param>
        /// <param name="newAuthor">Updated author information</param>
        /// <response code="204">Returns nothing</response>
        /// <response code="404">If author was not found.</response>                
        [HttpPut("{id:int}")]
        [SwaggerResponse(StatusCodes.Status204NoContent)]
        [SwaggerResponse(StatusCodes.Status404NotFound)]
        public async Task<ActionResult> PutAsync(int id, [Required] PutAuthorRequest newAuthor)
        {
            if (await authorService.GetByIdAsync(id) == null)
            {
                return NotFound();
            }

            await authorService.UpdateAsync(id, newAuthor);

            return NoContent();
        }
        /// <summary>
        /// Deletes author whose Id matches provided
        /// </summary>
        /// <remarks>
        /// 
        ///     DELETE /authors/5
        ///     
        /// </remarks>
        /// <returns>Returns nothing</returns>
        /// <param name="id">Id of an author</param>
        /// <response code="204">Returns nothing</response>
        /// <response code="404">If author was not found.</response>      
        [HttpDelete("{id:int}")]
        [SwaggerResponse(StatusCodes.Status204NoContent)]
        [SwaggerResponse(StatusCodes.Status404NotFound)]
        public async Task<ActionResult> DeleteAsync(int id)
        {
            if (await authorService.GetByIdAsync(id) == null)
            {
                return NotFound();
            }

            await authorService.DeleteAsync(id);

            return NoContent();
        }
    }
}
