﻿using BooksApi.Constants;
using BooksApi.Requests;
using BooksApi.Responses;
using BooksApi.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;

namespace BooksApi.Controllers
{
    [Route("api/coupons")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Authorize(Policy = AuthorizationPolicies.AdminPolicy)]
    public class CouponController : ControllerBase
    {
        private readonly BooksApiCouponService couponService;

        public CouponController(BooksApiCouponService couponService)
        {
            this.couponService = couponService;
        }
        /// <summary>
        /// Returns a list of coupons.
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// 
        ///     GET /coupons
        /// 
        /// </remarks>
        /// <response code="200">Returns list of coupons.</response>
        /// <response code="404">If coupons not found.</response>       
        [HttpGet]
        [SwaggerResponse(StatusCodes.Status200OK, Type = typeof(GetCouponResponse))]
        [SwaggerResponse(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<IEnumerable<GetCouponResponse>>> GetAsync()
        {
            var res = await couponService.GetAsync();

            if (res.ToList().Count < 1)
            {
                return NotFound();
            }

            return Ok(res);
        }
        /// <summary>
        /// Returns the coupon that matches unique id.
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// 
        ///     GET /coupons/4
        /// 
        /// </remarks>
        /// <param name="id">Unique id of the coupon</param>
        /// <response code="200">Returns coupon that matched unique id</response>
        /// <response code="404">If coupon not found.</response>               
        [HttpGet("{id:int}", Name = nameof(GetCouponByIdAsync))]
        [SwaggerResponse(StatusCodes.Status200OK, Type = typeof(GetCouponResponse))]
        [SwaggerResponse(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<GetCouponResponse>> GetCouponByIdAsync(int id)
        {
            var res = await couponService.GetByIdAsync(id);

            if (res == null)
            {
                return NotFound();
            }

            return Ok(res);
        }
        /// <summary>
        /// Creates a new coupon.
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// 
        ///     POST /coupons
        ///     {
        ///         "userId": 10,
        ///         "validNum" : 5,
        ///         "procentOff": 10
        ///     }
        /// 
        /// </remarks>
        /// <param name="newCoupon">Data for the new coupon.</param>
        /// <response code="201">Returns newly created coupon.</response>
        /// <response code="409">If user not found.</response>       
        [HttpPost]
        [SwaggerResponse(StatusCodes.Status201Created, Type = typeof(GetCouponResponse))]
        [SwaggerResponse(StatusCodes.Status409Conflict, Type = typeof(GetExceptionResponse))]
        public async Task<ActionResult<GetCouponResponse>> PostAsync([Required] PostCouponRequest newCoupon)
        {
            var res = await couponService.AddAsync(newCoupon);

            return CreatedAtRoute(nameof(GetCouponByIdAsync), new { res.Id }, res);
        }
        /// <summary>
        /// Updates a coupon.
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// 
        ///     POST /coupons/5
        ///     {
        ///         "userId": 10,
        ///         "validNum" : 5,
        ///         "procentOff": 10
        ///     }
        /// 
        /// </remarks>
        /// <param name="id">Unique id of the coupon.</param>
        /// <param name="newCoupon">Data for the updated coupon.</param>
        /// <response code="204">Returns nothing.</response>
        /// <response code="404">If coupon not found.</response>
        /// <response code="409">If user not found.</response>     
        [HttpPut("{id:int}")]
        [SwaggerResponse(StatusCodes.Status204NoContent)]
        [SwaggerResponse(StatusCodes.Status404NotFound)]
        [SwaggerResponse(StatusCodes.Status409Conflict, Type = typeof(GetExceptionResponse))]
        public async Task<ActionResult> UpdateAsync(int id, [Required] PostCouponRequest newCoupon)
        {
            if (await couponService.GetByIdAsync(id) == null)
            {
                return NotFound();
            }

            await couponService.UpdateAsync(id, newCoupon);

            return NoContent();
        }
        // PATCH api/coupons/5
        [HttpPatch("{id:int}")]
        public async Task<ActionResult> PatchAsync(int id, [Required] JsonPatchDocument patchDoc)
        {
            await couponService.PatchAsync(id, patchDoc);

            return NoContent();
        }
        /// <summary>
        /// Deletes a coupon.
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// 
        ///     DELETE /coupons/5
        /// 
        /// </remarks>
        /// <param name="id">Unique id of the coupon.</param>
        /// <response code="204">Returns nothing.</response>
        /// <response code="404">If coupon not found.</response>
        [HttpDelete("{id:int}")]
        [SwaggerResponse(StatusCodes.Status204NoContent)]
        [SwaggerResponse(StatusCodes.Status404NotFound)]
        public async Task<ActionResult> DeleteAsync(int id)
        {
            if (await couponService.GetByIdAsync(id) == null)
            {
                return NotFound();
            }

            await couponService.DeleteAsync(id);

            return NoContent();
        }
    }
}
