﻿using BooksApi.Constants;
using BooksApi.Requests;
using BooksApi.Responses;
using BooksApi.Responses.Book;
using BooksApi.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;

namespace BooksApi.Controllers
{
    [ApiController]
    [Route("api/users")]
    [Authorize]
    [Produces(MediaTypeNames.Application.Json)]
    public class UserController : ControllerBase
    {
        private readonly UserService userService;

        public UserController(UserService userService)
        {
            this.userService = userService;
        }
        /// <summary>
        /// Returns a list of users.
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// 
        ///     GET /users
        /// 
        /// </remarks>
        /// <response code="200">Returns list of users.</response>
        /// <response code="404">If users not found.</response>    
        [HttpGet]
        [SwaggerResponse(StatusCodes.Status200OK, Type = typeof(GetUserResponse))]
        [SwaggerResponse(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<IEnumerable<GetUserResponse>>> GetAsync()
        {
            var res = await userService.GetAsync();

            if (res.Count < 1)
            {
                return NotFound();
            }

            return Ok(res);
        }
        /// <summary>
        /// Returns user that matches unique id.
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// 
        ///     GET /users/4
        /// 
        /// </remarks>
        /// <param name="id">Unique id of the user</param>
        /// <response code="200">Returns user that matched unique id.</response>
        /// <response code="404">If user not found.</response>   
        [HttpGet("{id:int}")]
        [SwaggerResponse(StatusCodes.Status200OK, Type = typeof(GetUserResponse))]
        [SwaggerResponse(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<GetUserResponse>> GetUserByIdAsync(int id)
        {
            if (await userService.GetByIdAsync(id) == null)
            {
                return NotFound();
            }

            return Ok(await userService.GetByIdAsync(id));
        }
        /// <summary>
        /// Returns favorite items of the user that matches unique id.
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// 
        ///     GET /users/4/favorites
        /// 
        /// </remarks>
        /// <param name="id">Unique id of the user</param>
        /// <response code="200">Returns favorites for the user that matched unique id.</response>
        /// <response code="404">If user not found.</response>   
        [HttpGet("{id:int}/favorites")]
        [SwaggerResponse(StatusCodes.Status200OK, Type = typeof(GetBookResponse))]
        [SwaggerResponse(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<IEnumerable<GetBookResponse>>> GetUserFavoritesAsync(int id)
        {
            var res = await userService.GetUserFavoritesAsync(id);

            if (res.ToList().Count < 1)
            {
                return NotFound();
            }

            return Ok(res);
        }
        /// <summary>
        /// Updates avatar for the user that matches unique id.
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// 
        ///     PUT /users/4/avatar
        ///     "avatar" : "some file"
        /// 
        /// </remarks>
        /// <param name="id">Unique id of the user</param>
        /// <param name="avatar">Avatar image file for the user.</param>
        /// <response code="204">Returns nothing.</response>
        /// <response code="404">If user not found.</response>
        /// <response code="409">If avatar upload failed.</response>   
        [HttpPut("{id:int}/avatar")]
        [Consumes("multipart/form-data")]
        [SwaggerResponse(StatusCodes.Status204NoContent)]
        [SwaggerResponse(StatusCodes.Status404NotFound)]
        [SwaggerResponse(StatusCodes.Status409Conflict, Type = typeof(GetExceptionResponse))]
        public async Task<ActionResult> PutAvatarAsync(int id, IFormFile avatar)
        {
            if (await userService.GetByIdAsync(id) == null)
            {
                return NotFound();
            }

            await userService.UpdateAvatarAsync(id, avatar);

            return NoContent();
        }
        /// <summary>
        /// Updates avatar for the user that matches unique id.
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// 
        ///     PUT /users/4/avatar
        ///     "avatar" : "pa9disiapod=="
        /// 
        /// </remarks>
        /// <param name="id">Unique id of the user</param>
        /// <param name="avatar">Avatar base64String image for the user.</param>
        /// <response code="204">Returns nothing.</response>
        /// <response code="404">If user not found.</response>
        /// <response code="409">If avatar upload failed.</response>  
        [HttpPut("{id:int}/avatar")]
        [Consumes(MediaTypeNames.Application.Json)]
        [SwaggerResponse(StatusCodes.Status204NoContent)]
        [SwaggerResponse(StatusCodes.Status404NotFound)]
        [SwaggerResponse(StatusCodes.Status409Conflict, Type = typeof(GetExceptionResponse))]
        public async Task<ActionResult> PutAvatarAsync(int id, PostAvatarRequest avatar)
        {
            if (await userService.GetByIdAsync(id) == null)
            {
                return NotFound();
            }

            await userService.UpdateAvatarAsync(id, avatar);

            return NoContent();
        }
        /// <summary>
        /// Updates user that matches unique id.
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// 
        ///     PUT /users/4
        ///     "username" : "testUser",
        ///     "email" : "a@mail.com",
        ///     "password" : "password123"
        /// 
        /// </remarks>
        /// <param name="id">Unique id of the user</param>
        /// <param name="updatedUser">Data for the updated user.</param>
        /// <response code="204">Returns nothing.</response>
        /// <response code="404">If user not found.</response>
        /// <response code="409">If user update failed.</response>          
        [HttpPut("{id:int}")]
        [SwaggerResponse(StatusCodes.Status204NoContent)]
        [SwaggerResponse(StatusCodes.Status404NotFound)]
        [SwaggerResponse(StatusCodes.Status409Conflict, Type = typeof(GetExceptionResponse))]
        public async Task<ActionResult> PutAsync(int id, PutUserRequest updatedUser)
        {
            if (await userService.GetByIdAsync(id) == null)
            {
                return NotFound();
            }

            await userService.UpdateAsync(id, updatedUser);

            return NoContent();
        }
        /// <summary>
        /// Deletes user that matches unique id.
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// 
        ///     DELETE /users/4
        /// 
        /// </remarks>
        /// <param name="id">Unique id of the user</param>
        /// <response code="204">Returns nothing.</response>
        /// <response code="404">If user not found.</response>
        [HttpDelete("{id:int}")]
        [Authorize(Policy = AuthorizationPolicies.AdminPolicy)]
        public async Task<ActionResult> DeleteAsync(int id)
        {
            if (await userService.GetByIdAsync(id) == null)
            {
                return NotFound();
            }

            await userService.DeleteAsync(id);

            return NoContent();
        }
        /// <summary>
        /// Deletes avatar for user that matches unique id.
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// 
        ///     DELETE /users/4/avatar
        /// 
        /// </remarks>
        /// <param name="id">Unique id of the user</param>
        /// <response code="204">Returns nothing.</response>
        /// <response code="404">If user not found.</response>
        /// <response code="409">If user avatar not found.</response>
        [HttpDelete("{id:int}/avatar")]
        [SwaggerResponse(StatusCodes.Status204NoContent)]
        [SwaggerResponse(StatusCodes.Status404NotFound)]
        [SwaggerResponse(StatusCodes.Status409Conflict, Type = typeof(GetExceptionResponse))]
        public async Task<ActionResult> DeleteAvatarAsync(int id)
        {
            if (await userService.GetByIdAsync(id) == null)
            {
                return NotFound();
            }

            await userService.DeleteAvatarAsync(id);

            return NoContent();
        }
    }
}
