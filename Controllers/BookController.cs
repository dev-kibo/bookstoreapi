﻿using BooksApi.Constants;
using BooksApi.Models;
using BooksApi.Requests;
using BooksApi.Responses;
using BooksApi.Responses.Book;
using BooksApi.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Net.Mime;
using System.Threading.Tasks;


namespace BooksApi.Controllers
{
    [Route("api/books")]
    [ApiController]
    [Authorize(Policy = AuthorizationPolicies.AdminPolicy)]
    [Produces(MediaTypeNames.Application.Json)]
    public class BookController : ControllerBase
    {
        private readonly BookService bookService;

        public BookController(BookService bookService)
        {
            this.bookService = bookService;
        }
        /// <summary>
        /// Returns default 5 books sorted by Isbn in descending order
        /// </summary>
        /// <remarks>
        ///     
        ///     GET /books
        ///     GET /books?page=1
        ///     GET /books?perPage=10
        ///     GET /books?filter=title
        ///     GET /books?filter=isbn
        /// 
        /// </remarks>
        /// <param name="pageQuery">Page number and number of results per page</param>
        /// <param name="sortQuery">Sort results in ascending or descending order, Order results by Isbn, Title, Price, Stock, Status and PublishedAt date</param>
        /// <param name="filterQuery">Filter by Title or Isbn</param>
        /// <returns>Returns list of books</returns>
        /// <response code="200">Returns list of books</response>
        /// <response code="404">If no books are found</response>
        [HttpGet]
        [AllowAnonymous]
        [SwaggerResponse(StatusCodes.Status200OK, Type = typeof(PagedResponse<Book>))]
        [SwaggerResponse(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<IEnumerable<GetBookResponse>>> GetAsync(
            [FromQuery] PaginationQuery pageQuery,
            [FromQuery] SortQuery sortQuery,
            [FromQuery] FilterQuery filterQuery)
        {
            var res = await bookService.GetAsync(pageQuery, sortQuery, filterQuery);

            if (res == null)
            {
                return NotFound();
            }

            return Ok(res);
        }
        /// <summary>
        /// Returns book that matches provided Id
        /// </summary>
        /// <remarks>
        ///     
        ///     GET /books/4
        /// 
        /// </remarks>
        /// <param name="id">Book Id</param>
        /// <returns>Returns book that matches Id</returns>
        /// <response code="200">Returns book that matches Id</response>
        /// <response code="404">If no book is found</response>
        [HttpGet("{id:int}", Name = nameof(GetBookByIdAsync))]
        [AllowAnonymous]
        [SwaggerResponse(StatusCodes.Status200OK, Type = typeof(GetBookResponse))]
        [SwaggerResponse(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<GetBookResponse>> GetBookByIdAsync(int id)
        {
            var book = await bookService.GetByIdAsync(id);

            if (book == null)
            {
                return NotFound();
            }

            return Ok(book);
        }
        /// <summary>
        /// Creates new book and author if book does not exist. Status can be Availble or Unavailable.
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// 
        ///     POST /books
        ///     {
        ///         "title" : "My first book",
        ///         "isbn": "12345678-1902",
        ///         "price": 2000.50,
        ///         "description" : "Some description",
        ///         "numberOfPages" : 400,
        ///         "stock": 700,
        ///         "status" : "available",
        ///         "publishedAt": "2020-02-01",
        ///         "author": {
        ///             "firstName" : "John",
        ///             "lastName": "Smith"
        ///         }
        ///     }
        /// 
        /// </remarks>
        /// <param name="book">New book information</param>
        /// <response code="201">Returns newly created book</response>
        /// <response code="409">If book exists.</response>
        [HttpPost]
        [SwaggerResponse(StatusCodes.Status201Created, Type = typeof(GetBookResponse))]
        [SwaggerResponse(StatusCodes.Status409Conflict, Type = typeof(GetExceptionResponse))]
        public async Task<ActionResult<GetBookResponse>> PostAsync([Required] PostBookRequest book)
        {
            var res = await bookService.AddAsync(book);

            return CreatedAtRoute(nameof(GetBookByIdAsync), new { res.Id }, res);
        }
        /// <summary>
        /// Update a book that matches unique Id. Status can be Availble or Unavailable.
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// 
        ///     PUT /books/4
        ///     {
        ///         "title" : "My first book",
        ///         "isbn": "12345678-1902",
        ///         "price": 2000.50,
        ///         "description" : "Some description",
        ///         "numberOfPages" : 400,
        ///         "image": null,
        ///         "stock": 700,
        ///         "status" : "available",
        ///         "publishedAt": "2020-02-01",
        ///         "author": {
        ///             "firstName" : "John",
        ///             "lastName": "Smith"
        ///         }
        ///     }
        /// 
        /// </remarks>
        /// <param name="newBook">Data for the book</param>
        /// <param name="id">Unique id of the book</param>
        /// <response code="204">Returns nothing</response>
        /// <response code="409">If book image not found or book image is not valid.</response>        
        /// <response code="404">If book not found.</response>        
        [HttpPut("{id:int}")]
        [SwaggerResponse(StatusCodes.Status204NoContent)]
        [SwaggerResponse(StatusCodes.Status404NotFound)]
        [SwaggerResponse(StatusCodes.Status409Conflict, Type = typeof(GetExceptionResponse))]
        public async Task<ActionResult> PutAsync(int id, [Required] PutBookRequest newBook)
        {
            if (await bookService.GetByIdAsync(id) == null)
            {
                return NotFound();
            }

            await bookService.UpdateAsync(id, newBook);

            return NoContent();
        }
        // PATCH api/books/5
        [HttpPatch("{id:int}")]
        // Validate patch document
        public async Task<ActionResult> PatchAsync(int id, [Required] JsonPatchDocument patchDoc)
        {
            if (patchDoc == null)
            {
                return BadRequest();
            }
            if (await bookService.GetByIdAsync(id) == null)
            {
                return NotFound();
            }

            await bookService.PatchAsync(id, patchDoc);

            return NoContent();
        }
        /// <summary>
        /// Deletes a book that matches unique Id.
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// 
        ///     DELETE /books/4
        ///     
        /// </remarks>
        /// <param name="id">Unique id of the book</param>
        /// <response code="204">Returns nothing</response>
        /// <response code="404">If book not found.</response>               
        [HttpDelete("{id:int}")]
        [Authorize(Policy = AuthorizationPolicies.AdminPolicy)]
        public async Task<ActionResult> DeleteAsync(int id)
        {
            if (await bookService.GetByIdAsync(id) == null)
            {
                return NotFound();
            }

            await bookService.DeleteAsync(id);

            return NoContent();
        }
    }
}
