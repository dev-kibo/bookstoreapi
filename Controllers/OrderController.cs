﻿using BooksApi.Constants;
using BooksApi.Requests;
using BooksApi.Responses;
using BooksApi.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;

namespace BooksApi.Controllers
{
    [ApiController]
    [Route("api/orders")]
    [Produces(MediaTypeNames.Application.Json)]
    [Authorize(Policy = AuthorizationPolicies.CustomerPolicy)]
    public class OrderController : ControllerBase
    {
        private readonly BooksApiOrderService orderService;

        public OrderController(BooksApiOrderService orderService)
        {
            this.orderService = orderService;
        }
        /// <summary>
        /// Returns a list of orders.
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// 
        ///     GET /orders
        /// 
        /// </remarks>
        /// <response code="200">Returns list of orders.</response>
        /// <response code="404">If orders not found.</response>       
        [HttpGet]
        [SwaggerResponse(StatusCodes.Status200OK, Type = typeof(GetOrderResponse))]
        [SwaggerResponse(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<IEnumerable<GetOrderResponse>>> GetAsync()
        {
            var res = await orderService.GetAsync();

            if (res.ToList().Count < 1)
            {
                return NotFound();
            }

            return Ok(res);
        }
        /// <summary>
        /// Returns order that matches unique id.
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// 
        ///     GET /orders/5
        /// 
        /// </remarks>
        /// <param name="id">Unique id of order.</param>
        /// <response code="200">Returns list of orders.</response>
        /// <response code="404">If orders not found.</response>       
        [HttpGet("{id:int}", Name = nameof(GetOrderByIdAsync))]
        [SwaggerResponse(StatusCodes.Status200OK, Type = typeof(GetOrderResponse))]
        [SwaggerResponse(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<GetOrderResponse>> GetOrderByIdAsync(int id)
        {
            var res = await orderService.GetByIdAsync(id);

            if (res == null)
            {
                return NotFound();
            }

            return Ok(res);
        }
        /// <summary>
        /// Returns statistic of application state. Cache updates every 30 seconds.
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// 
        ///     GET /orders/stats
        /// 
        /// </remarks>
        /// <response code="200">Returns statistic response.</response>
        [HttpGet("stats")]
        [Authorize(Policy = AuthorizationPolicies.AdminPolicy)]
        [ResponseCache(Duration = 30, VaryByQueryKeys = new string[] { "*" })]
        [SwaggerResponse(StatusCodes.Status200OK, Type = typeof(GetStatsResponse))]
        public async Task<ActionResult<GetStatsResponse>> GetStatsAsync()
        {
            var res = await orderService.GetStatsAsync();

            return Ok(res);
        }
        /// <summary>
        /// Updates order that matches unique id.
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// 
        ///     PUT /orders/5
        ///     {
        ///         "userId" : 5,
        ///         "cartId" : 4
        ///     }
        /// 
        /// </remarks>
        /// <param name="id">Unique id of order.</param>
        /// <param name="newOrder">Data of the updated order.</param>
        /// <response code="204">Returns nothing.</response>
        /// <response code="404">If order not found.</response>  
        [HttpPut("{id:int}")]
        [SwaggerResponse(StatusCodes.Status204NoContent)]
        [SwaggerResponse(StatusCodes.Status404NotFound)]
        public async Task<ActionResult> UpdateAsync(int id, PostOrderRequest newOrder)
        {
            if (await orderService.GetByIdAsync(id) == null)
            {
                return NotFound();
            }

            await orderService.UpdateAsync(id, newOrder);

            return NoContent();
        }
        /// <summary>
        /// Deletes order that matches unique id.
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// 
        ///     DELETE /orders/5
        /// 
        /// </remarks>
        /// <param name="id">Unique id of order.</param>
        /// <response code="204">Returns nothing.</response>
        /// <response code="404">If order not found.</response>
        [HttpDelete("{id:int}")]
        [SwaggerResponse(StatusCodes.Status204NoContent)]
        [SwaggerResponse(StatusCodes.Status404NotFound)]
        public async Task<ActionResult> DeleteAsync(int id)
        {
            if (await orderService.GetByIdAsync(id) == null)
            {
                return NotFound();
            }

            await orderService.DeleteAsync(id);

            return NoContent();
        }
    }
}
