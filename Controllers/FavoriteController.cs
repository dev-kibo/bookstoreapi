﻿using BooksApi.Requests;
using BooksApi.Responses;
using BooksApi.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using System.ComponentModel.DataAnnotations;
using System.Net.Mime;
using System.Threading.Tasks;

namespace BooksApi.Controllers
{
    [Route("api/favorites")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Authorize]
    public class FavoriteController : ControllerBase
    {
        private readonly FavoriteService favoriteService;

        public FavoriteController(FavoriteService favoriteService)
        {
            this.favoriteService = favoriteService;
        }
        /// <summary>
        /// Returns a favorite item that matches unique id.
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// 
        ///     GET /favorites/4
        /// 
        /// </remarks>
        /// <param name="id">Favorite unique id.</param>
        /// <response code="200">Returns favorite item.</response>
        /// <response code="404">If favorite item not found.</response>       
        [HttpGet("{id:int}", Name = nameof(GetFavoriteByIdAsync))]
        [SwaggerResponse(StatusCodes.Status200OK, Type = typeof(GetFavoriteResponse))]
        [SwaggerResponse(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<GetFavoriteResponse>> GetFavoriteByIdAsync(int id)
        {
            var res = await favoriteService.GetByIdAsync(id);

            if (res == null)
            {
                return NotFound();
            }

            return Ok(res);
        }
        /// <summary>
        /// Creates a new favorite item.
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// 
        ///     POST /favorites
        ///     {
        ///         "userId" : 1,
        ///         "bookId" : 5
        ///     }
        /// 
        /// </remarks>
        /// <param name="newFavorite">Data for the new favorite item.</param>
        /// <response code="201">Returns newly created favorite item.</response>
        /// <response code="409">If user or book not found.</response>       
        [HttpPost]
        [HttpGet("{id:int}", Name = nameof(GetFavoriteByIdAsync))]
        [SwaggerResponse(StatusCodes.Status201Created, Type = typeof(GetFavoriteResponse))]
        [SwaggerResponse(StatusCodes.Status409Conflict, Type = typeof(GetExceptionResponse))]
        public async Task<ActionResult<GetFavoriteResponse>> PostAsync([Required] PostFavoriteRequest newFavorite)
        {
            var res = await favoriteService.AddAsync(newFavorite);

            return CreatedAtRoute(nameof(GetFavoriteByIdAsync), new { res.Id }, res);
        }
        /// <summary>
        /// Deletes a favorite item.
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// 
        ///     DELETE /favorites/5
        /// 
        /// </remarks>
        /// <param name="id">Favorite unique id.</param>
        /// <response code="204">Returns nothing.</response>
        /// <response code="404">If favorite item not found.</response>       
        [HttpDelete("{id:int}")]
        [SwaggerResponse(StatusCodes.Status204NoContent)]
        [SwaggerResponse(StatusCodes.Status404NotFound)]
        public async Task<ActionResult> DeleteAsync(int id)
        {
            if (await favoriteService.GetByIdAsync(id) == null)
            {
                return NotFound();
            }

            await favoriteService.DeleteAsync(id);

            return NoContent();
        }
    }
}
