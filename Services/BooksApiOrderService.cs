﻿using AutoMapper;
using BooksApi.Constants;
using BooksApi.Data;
using BooksApi.Enums;
using BooksApi.Models;
using BooksApi.Requests;
using BooksApi.Responses;
using LazyCache;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Transactions;

namespace BooksApi.Services
{
    public class BooksApiOrderService
    {
        private readonly BooksApiContext context;
        private readonly IMapper mapper;
        private readonly IAppCache appCache;

        public BooksApiOrderService(
            BooksApiContext context,
            IMapper mapper,
            IAppCache appCache)
        {
            this.context = context;
            this.mapper = mapper;
            this.appCache = appCache;
        }
        // Get all
        public async Task<IEnumerable<GetOrderResponse>> GetAsync()
        {
            var res = await context.Orders
                .Include(o => o.Cart.CartBooks)
                .ThenInclude(cb => cb.Book)
                .ThenInclude(b => b.AuthorBooks)
                .ThenInclude(ab => ab.Author)
                .AsNoTracking()
                .ToListAsync();

            return mapper.Map<IEnumerable<GetOrderResponse>>(res);
        }
        // Get by id
        public async Task<GetOrderResponse> GetByIdAsync(int id)
        {
            var res = await context.Orders
               .Where(o => o.Id.Equals(id))
               .Include(o => o.Cart.CartBooks)
               .ThenInclude(cb => cb.Book)
               .ThenInclude(b => b.AuthorBooks)
               .ThenInclude(ab => ab.Author)
               .AsNoTracking()
               .FirstOrDefaultAsync();

            return mapper.Map<GetOrderResponse>(res);
        }
        // Get stats
        public async Task<GetStatsResponse> GetStatsAsync()
        {
            Func<Task<GetStatsResponse>> statsGetter = async () =>
            {
                using var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled);

                var ordersCount = await context.Orders
                    .AsNoTracking()
                    .CountAsync();

                var totalValue = await context.Orders
                    .AsNoTracking()
                    .Select(o => o.Total)
                    .SumAsync();

                var ordersPastMonth = await context.Orders
                    .Where(o => o.CreatedAt.Month < 30)
                    .CountAsync();

                var ordersPastSixMonths = await context.Orders
                    .Where(o => o.CreatedAt.Month < 180)
                    .CountAsync();

                var carts = await context.Carts
                    .Select(c => c.Status)
                    .ToListAsync();

                var unconfirmedOrders = carts
                    .Where(c => c.Equals(CartStatus.Unordered))
                    .Count();

                var confirmedOrders = carts
                    .Where(c => c.Equals(CartStatus.Ordered))
                    .Count();

                scope.Complete();

                return new GetStatsResponse
                {
                    TotalOrders = ordersCount,
                    TotalValue = totalValue,
                    OrdersInPastMonth = ordersPastMonth,
                    OrdersInPastSixMonths = ordersPastSixMonths,
                    ConfirmedOrders = confirmedOrders,
                    UnconfirmedOrders = unconfirmedOrders
                };
            };

            return await appCache.GetOrAddAsync(CacheKeys.OrderServiceGetStatsAsync, statsGetter, TimeSpan.FromSeconds(30));
        }
        // Add
        public async Task<GetOrderResponse> AddAsync(Order newOrder)
        {
            var order = await context.Orders.AddAsync(newOrder);
            await context.SaveChangesAsync();

            return await GetByIdAsync(order.Entity.Id);
        }
        // Update
        public async Task UpdateAsync(int id, PostOrderRequest newOrder)
        {
            var order = await context.Orders.FindAsync(id);

            mapper.Map(newOrder, order);

            await context.SaveChangesAsync();
        }
        // Delete
        public async Task DeleteAsync(int id)
        {
            var toDelete = await context.Orders.FindAsync(id);

            context.Orders.Remove(toDelete);

            await context.SaveChangesAsync();
        }
    }
}
