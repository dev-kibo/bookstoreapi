﻿using BooksApi.Constants;
using BooksApi.Exceptions;
using BooksApi.Models;
using BooksApi.Requests;
using BooksApi.Responses;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.Net.Http.Headers;
using System;
using System.Threading.Tasks;
using System.Transactions;

namespace BooksApi.Services
{
    public class AuthenticationService
    {
        private readonly UserService userService;
        private readonly BooksApiTokenService tokenService;
        private readonly SessionService sessionService;
        private readonly UserManager<ApplicationUser> userManager;
        private readonly IHttpContextAccessor httpContextAccessor;
        private readonly HashService hashService;

        public AuthenticationService(
            UserService userService,
            BooksApiTokenService tokenService,
            SessionService sessionService,
            UserManager<ApplicationUser> userManager,
            IHttpContextAccessor httpContextAccessor,
            HashService hashService)
        {
            this.userService = userService;
            this.tokenService = tokenService;
            this.sessionService = sessionService;
            this.userManager = userManager;
            this.httpContextAccessor = httpContextAccessor;
            this.hashService = hashService;
        }
        // Authenticate a user
        private async Task<GetAuthenticationResponse> AuthenticateAsync(ApplicationUser user)
        {
            var roles = await userManager.GetRolesAsync(user);
            var claims = await userManager.GetClaimsAsync(user);

            string clientInfo = httpContextAccessor.HttpContext.Request.Headers[HeaderNames.UserAgent];

            string refreshToken = tokenService.GenerateRefreshToken();

            Session session = new Session
            {
                UserId = user.Id,
                Client = clientInfo,
                RefreshTokenHash = hashService.HashSha256(refreshToken),
                RefreshedAt = DateTime.UtcNow
            };

            await sessionService.AddAsync(session);

            return new GetAuthenticationResponse
            {
                AccessToken = tokenService.GenerateJsonWebToken(session.UserId, session.Id, roles, claims),
                RefreshToken = refreshToken
            };
        }
        // Register
        public async Task<GetAuthenticationResponse> RegisterAsync(PostRegisterUserRequest newUser)
        {
            using var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled);

            var user = await userService.CreateUser(newUser);
            var result = await AuthenticateAsync(user);

            scope.Complete();

            return result;
        }
        // Login 
        public async Task<GetAuthenticationResponse> LoginAsync(PostUserLoginRequest loginCredentials)
        {
            var user = await userManager.FindByEmailAsync(loginCredentials.Email);

            if (user == null || !await userManager.CheckPasswordAsync(user, loginCredentials.Password))
            {
                throw new ServiceException(ServiceExceptionMessages.UserEmailOrPasswordIsWrong);
            }

            return await AuthenticateAsync(user);
        }
        // Logout
        public async Task LogoutAsync(int sessionId)
        {
            var session = await sessionService.GetByIdAsync(sessionId);

            if (session == null)
            {
                throw new ServiceException(ServiceExceptionMessages.SessionNotFound);
            }

            await sessionService.DeleteAsync(session);
        }
        // Refresh token
        public async Task<GetAuthenticationResponse> RefreshTokenAsync(string refreshToken)
        {
            var session = await sessionService.GetByRefreshTokenAsync(refreshToken);

            if (session == null)
            {
                throw new ServiceException(ServiceExceptionMessages.SessionNotFound);
            }

            refreshToken = tokenService.GenerateRefreshToken();

            await sessionService.UpdateRefreshTokenAsync(session.Id, hashService.HashSha256(refreshToken));

            var user = await userManager.FindByIdAsync(session.UserId.ToString());
            var roles = await userManager.GetRolesAsync(user);
            var claims = await userManager.GetClaimsAsync(user);

            return new GetAuthenticationResponse
            {
                AccessToken = tokenService.GenerateJsonWebToken(session.UserId, session.Id, roles, claims),
                RefreshToken = refreshToken
            };
        }
    }
}
