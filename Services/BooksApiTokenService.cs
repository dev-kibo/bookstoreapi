﻿using BooksApi.Constants;
using BooksApi.Models;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;

namespace BooksApi.Services
{
    public class BooksApiTokenService
    {
        private readonly JwtSettings jwtSettings;

        public BooksApiTokenService(JwtSettings jwtSettings)
        {
            this.jwtSettings = jwtSettings;
        }
        // Generate jwt
        public string GenerateJsonWebToken(
            int userId,
            int sessionId,
            IList<string> roles,
            IList<Claim> claims)
        {
            var tokenClaims = new List<Claim>
            {
                new Claim(AccessTokenClaims.UserId, userId.ToString()),
                new Claim(AccessTokenClaims.SessionId, sessionId.ToString())
            };

            tokenClaims.AddRange(roles.Select(role => new Claim(ClaimTypes.Role, role)));
            tokenClaims.AddRange(claims);

            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(tokenClaims),
                Expires = DateTime.UtcNow.AddHours(jwtSettings.Duration),
                SigningCredentials = GenerateSignInCredentials(jwtSettings.Secret),
                NotBefore = DateTime.UtcNow,
                Issuer = jwtSettings.Issuer
            };

            var tokenHandler = new JwtSecurityTokenHandler();
            var token = tokenHandler.CreateToken(tokenDescriptor);

            return tokenHandler.WriteToken(token);
        }
        // Generate refresh token
        public string GenerateRefreshToken()
        {
            using var cryptoServiceProvider = new RNGCryptoServiceProvider();
            var bytes = new byte[64];
            cryptoServiceProvider.GetBytes(bytes);

            return Convert.ToBase64String(bytes);
        }
        // Generate signin credentials
        private SigningCredentials GenerateSignInCredentials(string secretKey)
        {
            var tokenKey = Encoding.UTF8.GetBytes(secretKey);

            return new SigningCredentials(new SymmetricSecurityKey(tokenKey), SecurityAlgorithms.HmacSha256Signature);
        }
    }
}
