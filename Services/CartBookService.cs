﻿using AutoMapper;
using BooksApi.Constants;
using BooksApi.Data;
using BooksApi.Exceptions;
using BooksApi.Models;
using BooksApi.Requests;
using BooksApi.Requests.CartBook;
using BooksApi.Responses;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BooksApi.Services
{
    public class CartBookService
    {
        private readonly BooksApiContext context;
        private readonly IMapper mapper;
        private readonly BookService bookService;
        private readonly CartService cartService;

        public CartBookService(
            BooksApiContext context,
            IMapper mapper,
            BookService bookService,
            CartService cartService)
        {
            this.context = context;
            this.mapper = mapper;
            this.bookService = bookService;
            this.cartService = cartService;
        }
        // Get all
        public async Task<IEnumerable<GetCartBookResponse>> GetAsync(int cartId)
        {
            var res = await context.CartBooks
                .Where(cb => cb.CartId.Equals(cartId))
                .Include(cb => cb.Book)
                .AsNoTracking()
                .ToListAsync();

            if (res == null)
            {
                return null;
            }

            return mapper.Map<IEnumerable<GetCartBookResponse>>(res);
        }
        // Get by id
        public async Task<GetCartBookResponse> GetByIdAsync(int cartId, int itemId)
        {
            var res = await context.CartBooks
                .Where(cb => cb.CartId.Equals(cartId))
                .Include(cb => cb.Book)
                .AsNoTracking()
                .FirstOrDefaultAsync(cb => cb.Id.Equals(itemId));

            return mapper.Map<GetCartBookResponse>(res);
        }
        // Add
        public async Task<GetCartBookResponse> AddAsync(int cartId, PostCartBookRequest newCartBook)
        {
            if (await bookService.GetByIdAsync(newCartBook.BookId.Value) == null)
            {
                throw new ServiceException(ServiceExceptionMessages.BookNotFound);
            }
            if (await cartService.GetByIdAsync(cartId) == null)
            {
                throw new ServiceException(ServiceExceptionMessages.CartNotFound);
            }

            var cartBook = mapper.Map<CartBook>(newCartBook);
            cartBook.CartId = cartId;

            var cartItemAdded = await context.CartBooks.AddAsync(cartBook);

            await context.SaveChangesAsync();

            return await GetByIdAsync(cartId, cartItemAdded.Entity.Id);
        }
        // Update
        public async Task UpdateAsync(int id, int itemId, PutCartBookRequest newCartBook)
        {
            var cartBook = await context.CartBooks
                .Where(cb => cb.CartId.Equals(id))
                .FirstOrDefaultAsync(cb => cb.Id.Equals(itemId));

            mapper.Map(newCartBook, cartBook);

            await context.SaveChangesAsync();
        }
        // Delete
        public async Task DeleteAsync(int cartId, int itemId)
        {
            var toRemove = await context
                .CartBooks
                .Where(cb => cb.CartId.Equals(cartId))
                .FirstOrDefaultAsync(cb => cb.Id.Equals(itemId));

            context.CartBooks.Remove(toRemove);

            await context.SaveChangesAsync();
        }
    }
}
