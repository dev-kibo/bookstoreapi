﻿using AutoMapper;
using BooksApi.Constants;
using BooksApi.Data;
using BooksApi.Exceptions;
using BooksApi.Models;
using BooksApi.Requests;
using BooksApi.Responses;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BooksApi.Services
{
    public class FavoriteService
    {
        private readonly BooksApiContext context;
        private readonly IMapper mapper;
        private readonly BookService bookService;
        private readonly UserService userService;

        public FavoriteService(
            BooksApiContext context,
            IMapper mapper,
            BookService bookService,
            UserService userService)
        {
            this.context = context;
            this.mapper = mapper;
            this.bookService = bookService;
            this.userService = userService;
        }

        // Get all
        public async Task<IEnumerable<GetFavoriteResponse>> GetAsync()
        {
            var res = await context.Favorites
                .Include(f => f.User)
                .Include(f => f.Book)
                .AsNoTracking()
                .ToListAsync();

            return mapper.Map<IEnumerable<GetFavoriteResponse>>(res);
        }
        // Get by id
        public async Task<GetFavoriteResponse> GetByIdAsync(int id)
        {
            var res = await context.Favorites
                .Include(f => f.User)
                .Include(f => f.Book)
                .AsNoTracking()
                .FirstOrDefaultAsync(f => f.Id == id);

            return mapper.Map<GetFavoriteResponse>(res);
        }
        // Add
        public async Task<GetFavoriteResponse> AddAsync(PostFavoriteRequest newFavorite)
        {
            if (await bookService.GetByIdAsync(newFavorite.BookId.Value) == null)
            {
                throw new ServiceException(ServiceExceptionMessages.BookNotFound);
            }
            if (await userService.GetByIdAsync(newFavorite.UserId.Value) == null)
            {
                throw new ServiceException(ServiceExceptionMessages.UserNotFound);
            }

            var toAdd = context.Favorites.Add(mapper.Map<Favorite>(newFavorite));

            await context.SaveChangesAsync();

            return await GetByIdAsync(toAdd.Entity.Id);
        }
        // Remove
        public async Task DeleteAsync(int id)
        {
            var toRemove = await context.Favorites.FindAsync(id);

            context.Favorites.Remove(toRemove);

            await context.SaveChangesAsync();
        }
    }
}
