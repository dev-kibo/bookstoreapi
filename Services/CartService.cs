﻿using AutoMapper;
using BooksApi.Constants;
using BooksApi.Data;
using BooksApi.Enums;
using BooksApi.Exceptions;
using BooksApi.Models;
using BooksApi.Requests;
using BooksApi.Requests.Cart;
using BooksApi.Responses;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace BooksApi.Services
{
    public class CartService
    {
        private readonly BooksApiContext context;
        private readonly IMapper mapper;
        private readonly EmailSenderService emailSenderService;
        private readonly BooksApiCouponService couponService;
        private readonly UserService userService;

        public CartService(
            BooksApiContext context,
            IMapper mapper,
            EmailSenderService emailSenderService,
            BooksApiCouponService couponService,
            UserService userService)
        {
            this.context = context;
            this.mapper = mapper;
            this.emailSenderService = emailSenderService;
            this.couponService = couponService;
            this.userService = userService;
        }
        // Get by id
        public async Task<GetCartResponse> GetByIdAsync(int id)
        {
            var cart = await context.Carts
                .Include(c => c.Coupon)
                .Include(c => c.User)
                .AsNoTracking()
                .FirstOrDefaultAsync(c => c.Id.Equals(id));

            return mapper.Map<GetCartResponse>(cart);
        }
        // Add
        public async Task<GetCartResponse> AddAsync(PostCartRequest newCart)
        {
            if (newCart.CouponId.HasValue && await couponService.GetByIdAsync(newCart.CouponId.Value) == null)
            {
                throw new ServiceException(ServiceExceptionMessages.CouponNotFound);
            }
            if (await userService.GetByIdAsync(newCart.UserId.Value) == null)
            {
                throw new ServiceException(ServiceExceptionMessages.UserNotFound);
            }
            if (await HasCartAsync(newCart))
            {
                throw new ServiceException(ServiceExceptionMessages.CartExists);
            }

            var cart = mapper.Map<Cart>(newCart);

            var response = await context.Carts.AddAsync(cart);
            await context.SaveChangesAsync();

            return await GetByIdAsync(response.Entity.Id);
        }
        // Delete
        public async Task DeleteAsync(int id)
        {
            var toRemove = await context.Carts.FindAsync(id);

            context.Carts.Remove(toRemove);

            await context.SaveChangesAsync();
        }
        // Update
        public async Task UpdateAsync(int cartId, PutCartRequest updatedCart)
        {
            if (await userService.GetByIdAsync(updatedCart.UserId.Value) == null)
            {
                throw new ServiceException(ServiceExceptionMessages.UserNotFound);
            }
            if (updatedCart.CouponId.HasValue && await couponService.GetByIdAsync(updatedCart.CouponId.Value) == null)
            {
                throw new ServiceException(ServiceExceptionMessages.CouponNotFound);
            }

            var cart = await context.Carts.FindAsync(cartId);

            mapper.Map(updatedCart, cart);

            await context.SaveChangesAsync();
        }
        // Delete coupon
        public async Task DeleteCouponFromCartAsync(int cartId)
        {
            var cart = await context.Carts.FindAsync(cartId);
            cart.CouponId = null;

            await context.SaveChangesAsync();
        }
        // Check if user already has open cart
        private async Task<bool> HasCartAsync(PostCartRequest cart)
        {
            return await context.Carts.AnyAsync(c => c.UserId.Equals(cart.UserId) && c.Status.Equals(CartStatus.Unordered));
        }
        // Calculate total for a cart
        public decimal CalculateTotalForACart(int cartId)
        {
            return context.CartBooks
                .Where(cb => cb.CartId.Equals(cartId))
                .Select(cb => cb.Book.Price * cb.Quantity)
                .Sum();
        }
        // Add Stripe payment id
        public async Task AddStripePaymentIntentId(int cartId, string paymentIntentId)
        {
            var cart = await context
                .Carts
                .FindAsync(cartId);

            cart.StripePaymentId = paymentIntentId;

            await context.SaveChangesAsync();
        }
        // Update cart status
        public async Task UpdateCartStatusAsync(Cart cart)
        {
            cart.Status = CartStatus.Ordered;

            await context.SaveChangesAsync();
        }
        // Notify customers
        public async Task NotifyCustomersAboutCartAsync()
        {
            var customersWithOrders = await context.Carts
                .Include(c => c.User)
                .Include(c => c.CartBooks)
                .ThenInclude(cb => cb.Book)
                .Where(c => c.Status == CartStatus.Unordered)
                .Select(c => new CustomerWithOrders()
                {
                    Customer = c.User,
                    Orders = c.CartBooks
                })
                .AsNoTracking()
                .ToListAsync();

            customersWithOrders.ForEach(async customerWithOrder => await emailSenderService.SendToCustomerAsync(customerWithOrder));
        }
    }
}
