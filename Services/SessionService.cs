﻿using BooksApi.Data;
using BooksApi.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace BooksApi.Services
{
    public class SessionService
    {
        private readonly BooksApiContext context;
        private readonly HashService hashService;

        public SessionService(
            BooksApiContext context,
            HashService hashService)
        {
            this.context = context;
            this.hashService = hashService;
        }
        // Get by id
        public async Task<Session> GetByIdAsync(int id)
        {
            return await context.Sessions
                .AsNoTracking()
                .Where(s => s.Id == id)
                .FirstOrDefaultAsync();
        }
        // Get by refresh token
        public async Task<Session> GetByRefreshTokenAsync(string refreshToken)
        {
            return await context.Sessions
                .AsNoTracking()
                .FirstOrDefaultAsync(s => s.RefreshTokenHash == hashService.HashSha256(refreshToken));
        }
        // Add
        public async Task AddAsync(Session newSession)
        {
            await context.Sessions.AddAsync(newSession);

            await context.SaveChangesAsync();
        }
        // Update
        public async Task UpdateRefreshTokenAsync(int sessionId, byte[] refreshTokenHash)
        {
            var session = await context.Sessions.FindAsync(sessionId);

            session.RefreshedAt = DateTime.UtcNow;
            session.RefreshTokenHash = refreshTokenHash;

            await context.SaveChangesAsync();
        }
        // Delete
        public async Task DeleteAsync(Session session)
        {
            context.Sessions.Remove(session);

            await context.SaveChangesAsync();
        }
    }
}
