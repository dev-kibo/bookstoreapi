﻿using AutoMapper;
using BooksApi.Constants;
using BooksApi.Data;
using BooksApi.Exceptions;
using BooksApi.Models;
using BooksApi.Requests;
using BooksApi.Responses;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BooksApi.Services
{
    public class BooksApiCouponService
    {
        private readonly BooksApiContext context;
        private readonly IMapper mapper;
        private readonly UserService userService;

        public BooksApiCouponService(
            BooksApiContext context,
            IMapper mapper,
            UserService userService)
        {
            this.context = context;
            this.mapper = mapper;
            this.userService = userService;
        }

        // GET all
        public async Task<IEnumerable<GetCouponResponse>> GetAsync()
        {
            var res = await context.Coupons
                .Include(c => c.User)
                .AsNoTracking()
                .ToListAsync();

            return mapper.Map<IEnumerable<GetCouponResponse>>(res);
        }
        // GET by id
        public async Task<GetCouponResponse> GetByIdAsync(int id)
        {
            var res = await context.Coupons
                .Include(c => c.User)
                .AsNoTracking()
                .FirstOrDefaultAsync(c => c.Id == id);

            return mapper.Map<GetCouponResponse>(res);
        }
        // Add new
        public async Task<GetCouponResponse> AddAsync(PostCouponRequest newCoupon)
        {
            if (await userService.GetByIdAsync(newCoupon.UserId.Value) == null)
            {
                throw new ServiceException(ServiceExceptionMessages.UserNotFound);
            }

            var coupon = mapper.Map<Coupon>(newCoupon);

            var addedCoupon = await context.Coupons.AddAsync(coupon);

            await context.SaveChangesAsync();

            return await GetByIdAsync(addedCoupon.Entity.Id);
        }
        // Update
        public async Task UpdateAsync(int id, PostCouponRequest newCoupon)
        {
            var coupon = await context.Coupons.FindAsync(id);

            mapper.Map(newCoupon, coupon);

            await context.SaveChangesAsync();
        }
        // Partial update
        public async Task PatchAsync(int id, JsonPatchDocument patchDoc)
        {
            var coupon = await context.Coupons.FindAsync(id);

            patchDoc.ApplyTo(coupon);

            await context.SaveChangesAsync();
        }
        // Delete
        public async Task DeleteAsync(int id)
        {
            var coupon = await context.Coupons.FindAsync(id);

            context.Coupons.Remove(coupon);

            await context.SaveChangesAsync();
        }
        // Decrease coupon valid number
        public async Task DecreaseCouponValidNumberAsync(int id)
        {
            var coupon = await context
                .Coupons
                .FindAsync(id);

            coupon.ValidNum--;

            await context.SaveChangesAsync();
        }
    }
}
