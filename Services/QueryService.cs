﻿using BooksApi.Models;
using System.Linq;

namespace BooksApi.Services
{
    public class QueryService
    {
        public IQueryable<Book> OrderByAscendingOrder(BookOrderingType queryOrder, IQueryable<Book> query)
        {
            IOrderedQueryable<Book> orderBy = null;

            orderBy = queryOrder switch
            {
                BookOrderingType.Isbn => query.OrderBy(b => b.Isbn),
                BookOrderingType.Title => query.OrderBy(b => b.Title),
                BookOrderingType.Price => query.OrderBy(b => b.Price),
                BookOrderingType.Stock => query.OrderBy(b => b.Stock),
                BookOrderingType.Status => query.OrderBy(b => b.Status),
                BookOrderingType.PublishedAt => query.OrderBy(b => b.PublishedAt),
                _ => query.OrderBy(b => b.Isbn),
            };

            return orderBy.AsQueryable();
        }

        public IQueryable<Book> OrderByDescendingOrder(BookOrderingType queryOrder, IQueryable<Book> query)
        {
            IOrderedQueryable<Book> orderBy = null;

            orderBy = queryOrder switch
            {
                BookOrderingType.Isbn => query.OrderByDescending(b => b.Isbn),
                BookOrderingType.Title => query.OrderByDescending(b => b.Title),
                BookOrderingType.Price => query.OrderByDescending(b => b.Price),
                BookOrderingType.Stock => query.OrderByDescending(b => b.Stock),
                BookOrderingType.Status => query.OrderByDescending(b => b.Status),
                BookOrderingType.PublishedAt => query.OrderByDescending(b => b.PublishedAt),
                _ => query.OrderBy(b => b.Isbn),
            };

            return orderBy;
        }
    }
}
