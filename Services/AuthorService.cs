﻿using AutoMapper;
using BooksApi.Data;
using BooksApi.Models;
using BooksApi.Requests;
using BooksApi.Responses;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BooksApi.Services
{
    public class AuthorService
    {
        private readonly BooksApiContext context;
        private readonly IMapper mapper;

        public AuthorService(BooksApiContext context, IMapper mapper)
        {
            this.context = context;
            this.mapper = mapper;
        }

        // GET all
        public async Task<IEnumerable<GetAuthorResponse>> GetAsync()
        {
            var res = await context
                .Authors
                .AsNoTracking()
                .ToListAsync();

            return mapper.Map<IEnumerable<GetAuthorResponse>>(res);
        }
        // GET by id
        public async Task<GetAuthorResponse> GetByIdAsync(int id)
        {
            var res = await context
                .Authors
                .AsNoTracking()
                .FirstOrDefaultAsync(a => a.Id.Equals(id));

            return mapper.Map<GetAuthorResponse>(res);
        }
        // Get author id
        public async Task<Author> GetAuthorAsync(Author author)
        {
            return await context.Authors
                .Where(a => a.FirstName.Equals(author.FirstName) && a.LastName.Equals(author.LastName))
                .FirstOrDefaultAsync();
        }
        // Check if author exists
        public async Task<bool> HasAuthorAsync(Author author)
        {
            return await context
                .Authors
                .AnyAsync(a => a.FirstName.Equals(author.FirstName) && a.LastName.Equals(author.LastName));
        }
        // Add
        public async Task<Author> AddAsync(Author author)
        {
            await context.Authors.AddAsync(author);

            await context.SaveChangesAsync();

            return author;
        }
        // Update
        public async Task UpdateAsync(int id, PutAuthorRequest newAuthor)
        {
            var author = await context
                .Authors
                .FindAsync(id);

            mapper.Map(newAuthor, author);

            await context.SaveChangesAsync();
        }
        // Delete
        public async Task DeleteAsync(int id)
        {
            var author = await context.Authors.FindAsync(id);

            context.Authors.Remove(author);

            await context.SaveChangesAsync();
        }
    }
}
