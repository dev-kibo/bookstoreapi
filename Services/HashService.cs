﻿using System.Security.Cryptography;
using System.Text;

namespace BooksApi.Services
{
    public class HashService
    {
        // Hash string
        public byte[] HashSha256(string input)
        {
            using var sha256 = new SHA256Managed();

            return sha256.ComputeHash(Encoding.UTF8.GetBytes(input));
        }
    }
}
