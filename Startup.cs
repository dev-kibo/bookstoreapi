using AutoMapper;
using BooksApi.Data;
using BooksApi.Extensions;
using BooksApi.Filters;
using BooksApi.Models;
using FluentValidation.AspNetCore;
using Hangfire;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace BooksApi
{
    public class Startup
    {
        private readonly IConfiguration configuration;

        public Startup(IConfiguration configuration)
        {
            this.configuration = configuration;
        }
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSingletonConfigurationModel<EmailMetadata>(configuration.GetSection("EmailMetadata"));
            services.AddSingletonConfigurationModel<JwtSettings>(configuration.GetSection("JwtSettings"));
            services.AddSingletonConfigurationModel<StripeSettings>(configuration.GetSection("StripeSettings"));

            services.AddSwaggerConfiguration();

            services.AddResponseCaching();

            services.AddAuthenticationExtension(configuration.GetSection("JwtSettings").Get<JwtSettings>());

            services.AddAuthorizationExtension();

            services.AddHangfireExtension(configuration.GetConnectionString("SQL2019"));

            services.AddRazorPages();

            services.AddControllers(options =>
            {
                options.Filters.Add(typeof(ValidateModelFilter));
            })
                .ConfigureApiBehaviorOptions(options =>
                {
                    options.SuppressModelStateInvalidFilter = true;
                    options.SuppressMapClientErrors = true;
                })
                .AddNewtonsoftJson()
                .AddFluentValidation(fv => fv.RegisterValidatorsFromAssemblyContaining<Startup>());

            services.AddIdentityExtension();

            services.AddDbContext<BooksApiContext>(options => options.UseSqlServer(configuration.GetConnectionString("SQL2019")));

            services.AddAutoMapper(typeof(Startup));

            services.AddLazyCache();

            services.AddApplicationServices();
        }

        public void Configure(
            IApplicationBuilder app,
            IWebHostEnvironment env,
            BooksApiContext context,
            IRecurringJobManager recurringJobManager)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseExceptionHandler("/api/error");

            app.UseSwagger();

            app.UseSwaggerUI(options =>
            {
                options.SwaggerEndpoint("v1/swagger.json", "BooksApi");
                options.DefaultModelExpandDepth(0);
                options.DefaultModelsExpandDepth(-1);
            });

            context.Database.Migrate();

            app.UseRouting();

            app.UseAuthentication();

            app.UseAuthorization();

            app.UseResponseCaching();

            app.UseResponseHeaderConfiguration();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                endpoints.MapHangfireDashboard();
                endpoints.MapSwagger();
            });

            //Hangfire job
            // recurringJobManager.AddOrUpdate("customersNotify", () => cartService.NotifyCustomersAboutCartAsync(), Cron.Minutely);
        }
    }
}
