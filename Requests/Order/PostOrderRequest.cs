﻿namespace BooksApi.Requests
{
    public class PostOrderRequest
    {
        public int? UserId { get; set; }

        public int? CartId { get; set; }
    }
}
