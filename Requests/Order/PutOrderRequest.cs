﻿namespace BooksApi.Requests
{
    public class PutOrderRequest
    {
        public int? UserId { get; set; }

        public int? CartId { get; set; }
    }
}
