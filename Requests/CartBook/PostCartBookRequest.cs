﻿namespace BooksApi.Requests
{
    public class PostCartBookRequest
    {
        public int? BookId { get; set; }

        public int? Quantity { get; set; }
    }
}
