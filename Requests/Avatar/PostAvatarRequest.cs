﻿namespace BooksApi.Requests
{
    public class PostAvatarRequest
    {
        public string Avatar { get; set; }
    }
}
