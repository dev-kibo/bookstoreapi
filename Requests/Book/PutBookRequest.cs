﻿using BooksApi.Models;
using System;

namespace BooksApi.Requests
{
    public class PutBookRequest
    {
        public string Title { get; set; }

        public string Isbn { get; set; }

        public decimal? Price { get; set; }

        public string Description { get; set; }

        public string Image { get; set; }

        public int? NumberOfPages { get; set; }

        public int? Stock { get; set; }

        public BookStatus? Status { get; set; }

        public DateTime? PublishedAt { get; set; }
    }
}
