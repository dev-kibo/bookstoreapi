﻿using BooksApi.Models;
using BooksApi.Requests;
using System;

namespace BooksApi.Responses.Book
{
    public class PostBookRequest
    {
        public string Title { get; set; }

        public string Isbn { get; set; }

        public decimal? Price { get; set; }

        public string Description { get; set; }

        public string Image { get; set; }

        public int? NumberOfPages { get; set; }

        public int? Stock { get; set; }

        public BookStatus? Status { get; set; }

        public DateTime? PublishedAt { get; set; }

        public PostAuthorRequest Author { get; set; }
    }
}
