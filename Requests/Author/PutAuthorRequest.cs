﻿namespace BooksApi.Requests
{
    public class PutAuthorRequest
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }
    }
}
