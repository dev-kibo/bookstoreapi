﻿namespace BooksApi.Requests
{
    public class PostAuthorRequest
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }
    }
}
