﻿namespace BooksApi.Requests
{
    public class PostCartRequest
    {
        public int? CouponId { get; set; }

        public int? UserId { get; set; }
    }
}
