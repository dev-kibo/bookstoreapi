﻿namespace BooksApi.Requests
{
    public class PostUserLoginRequest
    {
        public string Email { get; set; }

        public string Password { get; set; }
    }
}
