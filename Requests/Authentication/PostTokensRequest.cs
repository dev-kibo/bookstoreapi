﻿namespace BooksApi.Requests
{
    public class PostTokensRequest
    {
        public string Token { get; set; }

        public string RefreshToken { get; set; }
    }
}
